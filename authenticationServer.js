require("dotenv").config()
const express = require("express")
const app = express()
const port = 6000
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt")
const mysql = require("mysql")
app.use(express.json())


//connect to database
const mysqlConnection = mysql.createConnection({
	host: process.env.DATABASE_HOST,
	user: process.env.DATABASE_USER,
	password: process.env.DATABASE_PASSWORD,
	database: process.env.DATABASE_NAME,
	port: process.env.DATABASE_PORT,
	multipleStatements:true,
})


//database connection
mysqlConnection.connect((err) => {
	if(err) {
		console.log("Unable to connect database",err)
	} 
		console.log("connected to database") 
})



app.post("/signup",async (req,res) => {
	const {firstName,lastName,email,password} = req.body;
	
	if(!email || !password || !isValidEmail(email) || !isValidPassword(password)) {
		res.status(401).json("firstName,lastName,email,password mandatory and password should have numbers and letters")
		return 
	}

	//check if user  already exist
	let sql = `SELECT * FROM users WHERE email="${email}"`
	mysqlConnection.query(sql,async (err,user) => {
		if(err) throw err;
		user = JSON.parse(JSON.stringify(user))[0]
		 
		try {
			//if user already exist ,send the user exist message 
			if(user){
				res.status(401).json("user already exist")
				return
			}
		} catch (error) {
			res.status(401).json("something went wrong")
		}

		// if no user is found, create a new user
		const salt =  await bcrypt.genSalt(12)
		const hashedPassword = await bcrypt.hash(password,salt)
		sql = `insert into users set ?`
		let row = {firstName,lastName,email,password:hashedPassword}
		mysqlConnection.query(sql,row,(err,result) => {
			if(err) throw err
			console.log(result)
			res.json(result)
		})
	}) //mysqlConnection
}) //post


app.post("/login",  (req,res) => {
 	const {email,password} = req.body
	if(!email || !password)
		return res.status(401).json("email and password are required")

	const sql = `SELECT * FROM users WHERE email="${email}"`


	 mysqlConnection.query(sql,async (err,user) => {
		 if(err) throw err;
		 user = JSON.parse(JSON.stringify(user))[0]
		 console.log("user found", user)
		 try {
			//  console.log("incoming password is ",password)
			//  console.log("  user.password  is ",user.password)
			//  let salt = user.password.split(".")[0]
			//  console.log("  salt  is ",salt)
			//  console.log("stored hashed password is ",user.password)

			 console.log("incoming hashed password is ",await bcrypt.hash(password,salt))
			 if(await  bcrypt.compare(password,user.password)) {
				const accessToken = generateAccessToken(user)
				const  refreshToken = jwt.sign(user,process.env.REFRESH_TOKEN_SECRET)
				res.json({acccessToken:accessToken, refreshToken:refreshToken})
			 }

			//  res.json(user)
		 } catch (error) {
			 res.status(401).json("invalid creds")
		 }
	 })
	// const accessToken = jwt.sign(user,process.env.ACCESS_TOKEN_SECRET)
    
})


app.post('/getToken',(req,res) => {
    const  refreshToken =  req.body.refreshToken
    console.log("token refresh== ",refreshToken)
    jwt.verify(refreshToken,process.env.REFRESH_TOKEN_SECRET, (err,user) => {
        if(err)return  res.json("Invalid User",403)
        const accessToken = generateAccessToken({id:user.id,name:user.name})
        res.json({acccessToken:accessToken})
    })
})


//validate password
function isValidPassword(password) {
	return (
	// /[\@\#\$\%\^\&\*\(\)\_\+\!]/.test(password)  && 
	/[a-z]/.test(password)
	 && /[0-9]/.test(password) 
	//  &&  /[A-Z]/.test(password)
	)
}

//validate email
function isValidEmail(email) {
	return (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email))
}


function authenticateUserToken(req,res,next)  {
	const authHeader = req.headers['authorization']
	const token = authHeader && authHeader.split(' ')[1]
	if(!token) return res.json("Invalid user",401)

	jwt.verify(token,process.env.ACCESS_TOKEN_SECRET,(error,user) => {
		if(error) return res.json("You are unauthorized to perform ",401)
		req.user = user
		next()
		// return
	})
}


function generateAccessToken(user) {
    return jwt.sign(user,process.env.ACCESS_TOKEN_SECRET,{expiresIn: '60m'})
}

app.listen(port,() => {
	console.log("server is listening on port ",port)
})