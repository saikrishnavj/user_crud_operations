require("dotenv").config()
const express = require("express")
const mysql = require("mysql")
const app = express()
const port = 8000
const jwt = require("jsonwebtoken");
 

//connect to database
const mysqlConnection = mysql.createConnection({
	host: process.env.DATABASE_HOST,
	user: process.env.DATABASE_USER,
	password: process.env.DATABASE_PASSWORD,
	database: process.env.DATABASE_NAME,
	port: process.env.DATABASE_PORT,
	multipleStatements:true,
})
app.use(express.json())


//database connection
mysqlConnection.connect((err) => {
	if(err) {
		console.log("Unable to connect database",err)
	} 
		console.log("connected to database") 
})



app.get("/getUsers",(req,res) => {
	  mysqlConnection.query("SELECT * from users", (err,rows,fields) => {
		if(err) throw  err;
		let results=JSON.parse(JSON.stringify(rows))
		console.log("rows are", results)
		res.json(results)			 		 
	 })
})


app.post('/createUser',authenticateUserToken, (req,res) => {
	// get the params
	const {firstName,lastName,password,email} = req.body

	//validate the params
	if(!email || !password || !isValidEmail(email) || !isValidPassword(password)) {
		console.log("all fields are mandatory")
		res.status(401).json("firstName,lastName,email,password mandatory and password should have numbers and letters")
		return 
	}

	//check if user  already exist
	let sql = `SELECT * FROM users WHERE email="${email}"`
	mysqlConnection.query(sql,async (err,user) => {
		if(err) throw err;
		user = JSON.parse(JSON.stringify(user))[0]
		 
		try {
			//if user already exist ,send the user exist message 
			if(user){
				res.status(401).json("user already exist")
				return
			}
		} catch (error) {
			res.status(401).json("something went wrong")
		}

		// else create a user

		const  row = {firstName,lastName,password,email}
		let sql = "INSERT INTO users SET ?"

		//insert the row
		mysqlConnection.query(sql,row,(err,result) => {
			if(err) throw("error",err);
			console.log(result)
			res.json(result)
		}) //mysqlConnection
	
	}) 	 //mysqlConnection
}) //post

app.post('/updateUser/:id',authenticateUserToken, async (req,res) => {
	// get the user_id
	const user_id = parseInt(req.params.id)
	
	//get the updated fields
	const {firstName,lastName,password,email} = req.body

	//construct the query
	let  sql = "UPDATE users SET ";
	sql = (firstName && sql.concat(`firstName = "${firstName}" `  )) || sql 
	sql = (lastName && sql.concat(`,lastName = "${lastName}" `  )) || sql 
	sql = sql.concat(` WHERE id = ${user_id}`)
	
	console.log("===============")
	console.log("sql",sql)
	console.log("===============")

	mysqlConnection.query(sql,(err,result) => {
		if(err) throw err;
		console.log(result)
		res.json(result)
	})
 })

app.post('/deleteUser/:id',authenticateUserToken, (req,res) => { 
	// check if user exist
	const user_id = req.params.id
	let sql = `DELETE  FROM users WHERE id=${user_id}`
	mysqlConnection.query(sql,(err,result) => {
		if(err) throw err
		console.log(result)
	})
	//return the status whether user is deleted or not 
})


//validate password
function isValidPassword(password) {
	return (
	// /[\@\#\$\%\^\&\*\(\)\_\+\!]/.test(password)  && 
	/[a-z]/.test(password)
	 && /[0-9]/.test(password) 
	//  &&  /[A-Z]/.test(password)
	)
}

//validate email
function isValidEmail(email) {
	return (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email))
}

//authenticate user
function authenticateUserToken(req,res,next)  {
	const authHeader = req.headers['authorization']
	const token = authHeader && authHeader.split(' ')[1]
	if(!token) return res.status(401).json("Invalid creds")

	jwt.verify(token,process.env.ACCESS_TOKEN_SECRET,(error,user) => {
		if(error) return res.status(403).json("Invalid creds")
		req.user = user
		next()
		// return
	})
}
 

app.listen(port,() => {
	console.log("server is listening on port ",port)
})