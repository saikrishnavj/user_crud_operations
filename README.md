 For Database
 1) Create a table in database with name 'patchUsers'
 2) Create the table 'users' in the mysql ,copy the below table in the mysql platform

CREATE TABLE `patchUsers`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(45) NULL,
  `lastName` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `password` VARCHAR(100) NULL,
  `role_id` VARCHAR(45) 2,
  PRIMARY KEY (`id`)); 

3) update the password and username of the database in the .env file of the project


For Server running
I have created two servers
 a) authentication server which takes care of signup and login and token generation and refresh token generation
 b) userCreationServer which does CRUD operation on users . This can be any products CRUD operations
  
 The following steps has be performed to do CRUD operations on users
 Install 'rest Client' in the VScode for better support

1)
    a) I'm using yarn package manager
    b)To run authentication server: npm run authStart
    c)To run userCreation server: npm start
 
2)To signup(Note password should contain letters and numbers)
   POST http://localhost:6000/signup
    Content-Type: application/json

    {
        "email": "sanjay@gmail.com",
        "password": "sanjay121",
        "firstName": "sanjay",
        "lastName": "abay"
    }

3) Next, login to server using

    POST http://localhost:6000/login
    Content-Type: application/json

    {
        "email": "sanjay@gmail.com",
        "password": "sanjay121"
    }
   
==>This returns an ACCESS_TOKEN  which is valid for 60min

4)Use the ACCESS_TOKEN  to make request

POST http://localhost:8000/createUser
Content-Type: application/json
Authorization: Bearer ACCESS_TOKEN

{
    "firstName": "mithun",
    "lastName": "biradar",
    "email": "mithun@gmail.com",
    "password":"123aads4456565"
}

5) To get users
GET http://localhost:8000/getUsers


6) To update users
POST http://localhost:8000/updateUser/2
Content-Type: application/json
Authorization: Bearer ACCESS_TOKEN

{
    "firstName": "desh",
    "lastName": "besh",
    "email": "kittu@gmail.com"
}

7)To delete users
POST http://localhost:8000/deleteUser/3
Content-Type: application/json
Authorization: Bearer ACCESS_TOKEN



